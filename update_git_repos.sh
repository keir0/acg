#!/bin/bash
FOLDER=~/git_repos/
EXCLUDE_REPOS=("notes")

# Check if the specified folder exists
if [ ! -d "$FOLDER" ]; then
    echo "Error: Directory $FOLDER not found."
    exit 1
fi

# Find all .git folders up to a maximum depth of 2 and update Git repositories
for git_folder in $(find "$FOLDER" -maxdepth 3 -type d -name ".git")
do

    repo="${git_folder%/.git}"  # Get the parent directory of the .git folder
    # Check if the repo is in the exclude list
    if [[ " ${EXCLUDE_REPOS[@]} " =~ " ${repo##*/} " ]]; then
        echo "Skipping excluded repository in $repo"
        break
    fi

    echo "Updating repository in $repo"
    # Change into the repository directory and run 'git pull'
    (cd "$repo" && git pull)
    echo "------------------------"
done
