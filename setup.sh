#!/bin/bash
######################## Global setup ########################
setup/misc.sh

ln -s ~/git_repos/acg/.vimrc ~/.vimrc
ln -s ~/git_repos/acg/.zshrc ~/.zshrc
ln -s ~/git_repos/acg/.p10k.zsh ~/.p10k.zsh
#Todo : zsh ? Copy vimrc, bashrc, etc...

######################## Terraform ########################
setup/terraform.sh


######################## AWS-CLI ########################
setup/aws-cli.sh


######################## Kubernetes ########################
setup/kubctl.sh