#!/bin/bash
#WiP

#terminator
sudo apt install terminator -y
sudo update-alternatives --config x-terminal-emulator

#Font for p10k
https://github.com/romkatv/powerlevel10k#meslo-nerd-font-patched-for-powerlevel10k

#Zsh
sudo apt install zsh
chsh -s $(which zsh)
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"


#AutoSuggestions
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

#p10k
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

# Copier config

