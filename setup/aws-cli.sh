#!/bin/bash
#Install AWS-CLI
sudo apt install unzip
if [[ ! -d /usr/local/aws-cli ]]; then
	curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "/tmp/awscliv2.zip"
	unzip /tmp/awscliv2.zip -d /tmp
	sudo /tmp/aws/install -i /usr/local/aws-cli -b /usr/local/bin
	export AWS_DEFAULT_REGION=us-east-1
	export AWS_DEFAULT_OUTPUT=yaml
	aws configure
	rm /tmp/awscliv2.zip
	rm /tmp/aws -rf
else
	echo "aws-cli already installed"
fi

#Enable autocomplete for AWS
if [[ -f ~/.bashrc ]]; then
  echo "complete -C '/usr/local/bin/aws_completer' aws" >>~/.bashrc
fi

if [[ -f ~/.zshrc ]]; then
  echo "Please enable the AWS plugin for OhMyZsh or add complete -C '/usr/local/bin/aws_completer' aws to your .zshrc_local file"
fi
