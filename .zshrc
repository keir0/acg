# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

#ZSH_THEME="robbyrussell"
ZSH_THEME="powerlevel10k/powerlevel10k"
#ZSH_THEME="jreese"

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
	git
	zsh-autosuggestions
  aws
  terraform
  ansible
  kubectl
  virtualenv
)
DISABLE_MAGIC_FUNCTIONS=true
source $ZSH/oh-my-zsh.sh

#Source files with config that doesn't need to be commited : Example :
#.zshrc_local   => Put here everything related to the local machine (Alias of installed programmes, autocomplete commandes, etc...)
#.zshrc_$NAME   => Custom function related to compagny $NAME that should stay private
for file in ~/.zshrc_*; do
    source "$file"
done
##################### aliases  ###########################
alias grbh="git rebase origin/HEAD --autostash"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
