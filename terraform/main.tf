terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

#Create 3 nodes for k8 cluster
resource "aws_instance" "kubMaster" {
  ami           = "ami-07d02ee1eeb0c996c" #Debian10
  instance_type = "t2.micro"
  tags = {
    Name = "kubMaster"
  }
}

resource "aws_instance" "kubWorker" {
  ami           = "ami-07d02ee1eeb0c996c" #Debian10
  instance_type = "t2.micro"
  count         = 2
  tags = {
    Name = "kubWorker"
  }
}

#Generate Ansible inventory file
resource "local_file" "ansible_inventory" {
  content = templatefile("inventory.template",
    {
      kubMaster  = aws_instance.kubMaster.*.public_ip
      kubWorkers = aws_instance.kubWorker.*.public_ip
    }
  )
  filename = "../ansible/inventories/k8/inventory"
}
