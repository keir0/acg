set encoding=utf-8
set autoread
set backspace=indent,eol,start
set incsearch
set hlsearch

" Basic vim settings
set visualbell
set noswapfile

" Set the terminal's title
set title

set autoindent
filetype indent on
syntax on
set tabstop=2
set sts=2
set sw=2
set expandtab

" Set paste shortcut
set pastetoggle=<F2>

"Auto close brackets, etc...
inoremap " ""<left>
inoremap ' ''<left>
inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>
inoremap {<CR> {<CR>}<ESC>O
inoremap {;<CR> {<CR>};<ESC>O

"Python Config
au! BufNewFile,BufReadPost *.{py} set filetype=python foldmethod=manual
autocmd FileType python setlocal ts=4 sts=4 sw=4 expandtab

"YAML Config & terraform
au! BufNewFile,BufReadPost *.{yaml,yml,tf} set filetype=yaml indentkeys-=0# indentkeys-=<:>
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

"Puppet
au! BufNewFile,BufReadPost *.{pp} set filetype=ruby indentkeys-=0# indentkeys-=<:>
autocmd FileType pp setlocal ts=2 sts=2 sw=2 expandtab

"Remove all trailing whitespace by pressing F5
nnoremap <F5> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>

" Show trailing whitespace and spaces before a tab:
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$\| \+\ze\t/
