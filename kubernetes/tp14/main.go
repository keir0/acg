package main

import (
	"net/http"
	"os"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

const CurrentVersion = "2.0-beta"

type PingResponse struct {
	Pong    bool      `json:"pong"`
	Time    time.Time `json:"time"`
	Version string    `json:version"`
}

func main() {

	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.GET("/", func(c echo.Context) error {
		return c.HTML(http.StatusOK, "Hello Ambient IT - Beta")
	})

	e.GET("/ping", func(c echo.Context) error {
		return c.JSON(http.StatusOK, &PingResponse{Pong: true, Time: time.Now().UTC(), Version: CurrentVersion})
	})

	httpPort := os.Getenv("HTTP_PORT")
	if httpPort == "" {
		httpPort = "8080"
	}

	e.Logger.Fatal(e.Start(":" + httpPort))
}
